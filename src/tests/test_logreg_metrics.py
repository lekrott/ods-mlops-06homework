""" Script for checking basic metrics for logistic regression,
    just to make sure everything is working
"""
import pandas as pd
from joblib import load

from src.features import split_X_y
from src.models import rocauc_score


# pylint: disable=C0103
def logreg_test_score(model, test_X, test_y):
    """Get score metrics for the test dataset

    Args:
        model: estimator to get scores for
        test_X (pd.DataFrame): test dataset independent features
        test_y (pd.DataFrame): test dataset labels

    Returns:
        Dict: ROC AUC metric scores for train and test datasets respectively
    """
    test_auc = rocauc_score(model, test_X, test_y)
    return {"test_rocauc": test_auc}


# pylint: disable=C0103
def test_logreg_metrics():
    """
    Pytest test to ensure logistic regression ROC AUC metrics
    for the test dataset is not less than 0.8
    """
    test_features = pd.read_csv('data/processed/test_logreg_features.csv')
    test_X, test_y = split_X_y(test_features)

    logreg_model = load('models/logreg.joblib')

    logreg_results = logreg_test_score(
        model=logreg_model,
        test_X=test_X,
        test_y=test_y,
    )
    print("logreg results: ", logreg_results)

    assert logreg_results["test_rocauc"] > 0.8
