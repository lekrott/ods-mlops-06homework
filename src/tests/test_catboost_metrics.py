""" Script for checking basic metrics for catboost,
    just to make sure everything is working
"""
import pandas as pd
from catboost import CatBoostClassifier

from src.features import split_X_y
from src.models import rocauc_score


def catboost_test_score(model, test_X, test_y):
    """Get score metrics for the test dataset

    Args:
        model: estimator to get scores for
        test_features (pd.DataFrame): test dataset features
        test_target (pd.DataFrame): test dataset labels

    Returns:
        Dict: ROC AUC metric score for the test dataset
    """
    test_auc = rocauc_score(model, test_X, test_y)
    return {"test_rocauc": test_auc}


def test_catboost_metrics():
    """pytest test checking if catboost ROC AUC metrics
    bot for train and test dataset are not less than 0.8
    """
    test_features = pd.read_csv('data/processed/test_catboost_features.csv')
    test_X, test_y = split_X_y(test_features)

    catboost_model = CatBoostClassifier()
    catboost_model.load_model('models/catboost.model')

    catboost_results = catboost_test_score(
        model=catboost_model,
        test_X=test_X,
        test_y=test_y,
    )
    print("catboost results: ", catboost_results)

    assert catboost_results["test_rocauc"] > 0.8
