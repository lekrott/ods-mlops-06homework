"""
CLI version of split_train_test function.
See arguments specification for details.
"""
import click
import pandas as pd

from src.data import split_train_test


@click.command()
@click.argument('whole_dataset_path', type=click.Path(exists=True))
@click.argument('train_path', type=click.Path())
@click.argument('test_path', type=click.Path())
def cli_split_train_test(whole_dataset_path: str, train_path: str, test_path: str):
    """
    Loads input dataset from .csv file,
    splits the whole dataset into train and test parts,
    then saves train and test parts to respective .csv files in output folder.

    Args:
        whole_dataset_path (str): path to input whole data .csv
        train_path (str): path to an output train .csv file
        test_path (str): path to an output test .csv file
    """
    whole_data = pd.read_csv(whole_dataset_path)

    train, test = split_train_test(whole_data)

    train.to_csv(train_path)
    test.to_csv(test_path)


# pylint: disable=E1120
if __name__ == '__main__':
    cli_split_train_test()
