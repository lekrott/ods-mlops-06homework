""" CLI version of clean_data function.
    See arguments specification for details.
"""
import click
import pandas as pd
from src.data import clean_data


@click.command()
@click.argument('raw_data_path', type=click.Path(exists=True))
@click.argument('cleaned_data_path', type=click.Path())
def cli_clean_data(raw_data_path: str, cleaned_data_path: str):
    """ Loads raw data .csv file,
        does all the necessary cleaning,
        then saves cleaned data to a .csv file.

    Args:
        raw_data_path (str): path to input raw data .csv
        cleaned_data_path (str): path to output cleaned data .csv
    """
    raw_data = pd.read_csv(raw_data_path)

    cleaned_data = clean_data(raw_data)

    cleaned_data.to_csv(cleaned_data_path)


# pylint: disable=E1120
if __name__ == '__main__':
    cli_clean_data()
