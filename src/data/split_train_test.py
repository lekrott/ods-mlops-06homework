""" Split original labeled dataset into train and test parts
"""
from sklearn.model_selection import train_test_split

SEED = 762


def split_train_test(data):
    """Splits input dataframe into parts used as train and tests datasets

    Args:
        data (pd.DataFrame): complete dataframe to be splitted

    Returns:
        pd.DataFrame, pd.DataFrame: a tuple consists of train and test parts
    """
    train, test = train_test_split(data, train_size=0.8, random_state=SEED)
    return train, test
