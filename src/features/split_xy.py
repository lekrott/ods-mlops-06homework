""" Splits complete feature set into independent variables (X) and target variable (y)
"""
TARGET_COLUMN = 'Churn'


# pylint: disable=C0103
def split_X_y(features):
    """Splits input features dataframe into X and y parts

    Args:
        features (pd.DataFrame): full set of feature columns

    Returns:
        tuple (X, y): X = independent variables dataframe, y = target variable vector
    """
    X = features.drop(TARGET_COLUMN, axis=1)
    y = features[TARGET_COLUMN]

    return X, y
