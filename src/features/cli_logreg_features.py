"""
CLI version of logreg_features function.
See arguments specification for details.
"""
import click
import pandas as pd

from src.features import logreg_features


@click.command()
@click.argument('input_data_path', type=click.Path(exists=True))
@click.argument('features_data_path', type=click.Path())
def cli_logreg_features(input_data_path: str, features_data_path: str):
    """
    Loads input data from .csv file,
    processes the data into a form suitable for logistic regression classifier,
    then saves processed features into a .csv file.

    Args:
        input_data_path (str): path to input data .csv
        features_data_path (str): path to processed features .csv
    """
    input_data = pd.read_csv(input_data_path)

    features = logreg_features(input_data)

    features.to_csv(features_data_path)


# pylint: disable=E1120
if __name__ == '__main__':
    cli_logreg_features()
