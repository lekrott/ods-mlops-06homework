"""Metrics to evaluate any kind of estimators
"""
from sklearn.metrics import roc_auc_score


def rocauc_score(model, features, target):
    """Calculates ROC AUC score for an estimator given agains test data given

    Args:
        model (estimator): a model to score
        features (pd.DataFrame): test features to score on
        target (pd.DataFrame): test labels to check against

    Returns:
        float: ROC AUC score value
    """
    predicted = model.predict_proba(features)[:, 1]
    auc = roc_auc_score(target, predicted)
    return auc
