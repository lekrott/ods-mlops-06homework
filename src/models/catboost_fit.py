""" Train Catboost classifier
"""
import catboost

CATBOOST_CATEGORY_COLUMNS = [
    "Sex", "IsSeniorCitizen", "HasPartner",
    "HasChild", "HasPhoneService", "HasMultiplePhoneNumbers",
    "HasInternetService", "HasOnlineSecurityService",
    "HasOnlineBackup", "HasDeviceProtection", "HasTechSupportAccess",
    "HasOnlineTV", "HasMovieSubscription", "HasContractPhone", "IsBillingPaperless",
    "PaymentMethod",
]

SEED = 762


def catboost_fit(features, target, params):
    """Fit catboost classifier on data given

    Args:
        features (pd.DataFrame): train features
        target (pd.Series): train labels
        params (Dict): hyperparameters for CatBoostClassifier

    Returns:
        estimator: fitted catboost model
    """
    catboost_estimator = catboost.CatBoostClassifier(
        cat_features=CATBOOST_CATEGORY_COLUMNS,
        iterations=params['iterations'],
        l2_leaf_reg=params['l2_leaf_reg'],
        learning_rate=params['learning_rate'],
        random_state=SEED,
        silent=True,
    )
    catboost_estimator.fit(features, target)

    return catboost_estimator
