"""
CLI version of logreg_fit function.
See arguments specification for details.
"""
import os
import click
import pandas as pd
from joblib import dump
from dotenv import load_dotenv

import mlflow
from mlflow.models.signature import infer_signature

from src.features import split_X_y
from src.models import logreg_fit, rocauc_score


# pylint: disable=C0103
# pylint: disable=R0801
@click.command()
@click.argument('train_features_path', type=click.Path(exists=True))
@click.argument('model_path', type=click.Path())
def cli_logreg_fit(train_features_path: str, model_path: str):
    """
    Loads train features from .csv file,
    splits features into X and y parts,
    fits logistic regression classifier,
    then saves fitted model to a file.

    Args:
        train_features_path (str): path to train features data .csv
        model_path (str): path to fitted model save file
    """
    load_dotenv()

    remote_server_uri = os.getenv('MLFLOW_TRACKING_URI')
    mlflow.set_tracking_uri(remote_server_uri)

    mlflow.set_experiment('logreg_fit')

    train_data = pd.read_csv(train_features_path)

    train_X, train_y = split_X_y(train_data)

    parameters = {
        'lr_max_iter': 1000,
        'gscv_param_grid': {"C": [100, 10, 1, 0.1, 0.01, 0.001]},
        'gscv_cv': 5,
        'gscv_scoring': 'roc_auc',
        'gscv_n_jobs': -1
    }

    model = logreg_fit(train_X, train_y, params=parameters)

    score = {
        'rocauc': rocauc_score(model, train_X, train_y)
    }

    signature = infer_signature(train_X, train_y)

    mlflow.log_params(parameters)
    mlflow.log_metrics(score)
    mlflow.sklearn.log_model(model,
                             'churn_logreg',
                             registered_model_name='churn.logreg',
                             signature=signature)

    dump(model, model_path)


# pylint: disable=E1120
if __name__ == '__main__':
    cli_logreg_fit()
