"""Fit logistic regression estimator with best hyperparameters
"""
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression

SEED = 762


# pylint: disable=C0103
def logreg_fit(X, y, params):
    """Grid searches for best hyperparameters and fits logistic regression model

    Args:
        features (pd.DataFrame or np.ndarray): train features to be fitted on
        target (pd.Series or np.ndarray): train labels verctor
        params (Dict): hyperparameters for LogisticRegression and GridSearchCV

    Returns:
        model: ready model fitted using best parametars found
    """
    estimator = LogisticRegression(max_iter=params['lr_max_iter'], random_state=SEED)

    logreg_estimator = GridSearchCV(
        estimator,
        param_grid=params['gscv_param_grid'],
        cv=params['gscv_cv'],
        scoring=params['gscv_scoring'],
        n_jobs=params['gscv_n_jobs'],
        verbose=1,
    )

    logreg_estimator.fit(X, y)

    return logreg_estimator
