""" setup.py for the Homework#04 project
"""
from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Homework #6',
    author='Yury Filimonov',
    license='',
)
